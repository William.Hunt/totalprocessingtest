# Total Processing Test

A simple golang API for Total Processing that has very simple login and register features



## Getting Started

### Dependencies

* Docker compose

### Installing

* Simply run docker-compose up --build in the main directory

### Executing program

* Once docker compose has completed then you need to go to localhost/CreateTable to initialise the database user table. This can also be used to delete any test data and start again.
* Once the table has been created, you need to go to localhost/register with a post body of something like this {
  "username": "testuser",
  "password": "testpassword"
  }.
* Then you need to login using a post request to localhost/login with the body of {
  "username": "testuser",
  "password": "testpassword"
  } which will return you a JWT.
* Take this JWT and add it as an authorisation header in the following format Bearer {"token":"\<JWT token>"} to a get request to localhost/TestAPI
* Examples of all these URL's can be found in the postman export file.
