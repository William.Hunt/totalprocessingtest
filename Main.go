package main

import (
	"flag"
	"github.com/gorilla/handlers"
	"net/http"
	"os"
	"runtime"
	_ "total-processing-test/general/handlers"
	"total-processing-test/general/router"
)

func main() {
	portPtr := flag.String("port", "25567", "The port to run the server on.")
	flag.Parse()
	go InitialiseHttpListener(portPtr)
	runtime.Goexit()
}

func InitialiseHttpListener(port *string) {
	headersOk := handlers.AllowedHeaders([]string{"X-Requested-With", "origin", "content-type", "Authorization", "authorization"})
	originsOk := handlers.AllowedOrigins([]string{"*"})
	methodsOk := handlers.AllowedMethods([]string{"GET", "HEAD", "POST", "PUT", "DELETE", "OPTIONS"})
	http.ListenAndServe(":"+*port, handlers.LoggingHandler(os.Stdout, handlers.CORS(originsOk, headersOk, methodsOk)(router.NewRouter())))
}
