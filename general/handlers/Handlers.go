package handlers

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"total-processing-test/general/database"
	"total-processing-test/general/router"
)

func init() {
	router.AddDef(router.Route{Method: "GET", Pattern: "/CreateTable", HandlerFunc: CreateTable})
	router.AddAuth(router.Route{Method: "GET", Pattern: "/TestAPI", HandlerFunc: ConnectToCopyAndPay})
}
func CreateTable(w http.ResponseWriter, r *http.Request) {
	tx, _, _, _ := database.GetPostData(r, database.MySql)
	defer tx.Rollback()
	database.RunDataChange(`drop table tbl_users`, tx)
	database.RunDataChange(`Create table tbl_users
(
    username           varchar(200),
    password        varchar(200)
)`, tx)
	tx.Commit()
	fmt.Fprintln(w, "table created successfully")
}

func ConnectToCopyAndPay(w http.ResponseWriter, r *http.Request) {
	postBody := []byte("entityId=8ac7a4ca759cd78501759dd759ad02df&amount=92.00&currency=EUR&paymentType=DB")

	responseBody := bytes.NewBuffer(postBody)
	//Leverage Go's HTTP Post function to make request
	resp, err := http.Post("https://eu-test.oppwa.com/v1/checkouts", "application/x-www-form-urlencoded", responseBody)
	resp.Header.Set("Authorization", "Bearer OGFjN2E0Y2E3NTljZDc4NTAxNzU5ZGQ3NThhYjAyZGR8NTNybThiSmpxWQ==")
	//Handle Error
	if err != nil {
		log.Fatalf("An Error Occured %v", err)
	}
	defer resp.Body.Close()
	//Read the response body
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatalln(err)
	}
	sb := string(body)
	fmt.Fprintln(w, sb)
}
