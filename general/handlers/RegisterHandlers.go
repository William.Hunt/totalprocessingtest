package handlers

import (
	"fmt"
	"net/http"
	"total-processing-test/general/database"
	"total-processing-test/general/router"
)

func init() {
	router.AddDef(router.Route{Method: "POST", Pattern: "/register", HandlerFunc: CreateAccount})
}

func CreateAccount(w http.ResponseWriter, r *http.Request) {
	tx, _, postData, params := database.GetPostData(r, database.MySql)
	defer tx.Rollback()
	sql := `insert into
  tbl_users (
    username,
    password
  )
values
  (?, ?)`
	params = append(params, postData["username"], postData["password"])
	database.RunDataChange(sql, tx, params...)
	tx.Commit()
	fmt.Fprintln(w, "successfully created account")
}
